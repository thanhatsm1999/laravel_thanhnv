<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUsers;
use DB;
use App\Models\User;
class UsersController extends Controller
{   
    private $users;
    public function __construct(User $users)
    {
        $this->users = $users;
    }
    public function create()
    {
        return view('Admin.Add');
    }
    public function store(CreateUsers $request)
    {
        $this->users->AddUsers($request->all());
        flash('Thêm mới người dùng thành công');
        return \redirect(route('user.index'));
    }
    public function index()
    {
        $user = $this->users->getall();
        return view('Admin.DanhsachUsers', ['users' => $user]);
    }
}
