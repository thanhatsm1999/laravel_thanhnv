<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUsers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'mai_address' => 'required|email|unique:users,mai_address',
            'password' => 'required|max:255',
            'confirm_password' => 'required|same:password',
            'address' => 'required|max:255',
            'phone' => 'required|not_regex:/[a-z]/|max:15',
        ];
    }
}
