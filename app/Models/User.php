<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Model
{
    use SoftDeletes;
    protected $table = "users";

    protected $fillable = [
        'name',
        'mai_address',
        'address',
        'password',
        'phone',
    ];
    protected $dates = ['deleted_at'];

    public function AddUsers($request) 
    {
        return $this->create($request);
    }
    public function getall()
    {
        return User::paginate(20);
    }
}
