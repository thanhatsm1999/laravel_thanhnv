<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Users::class, function (Faker $faker) {
    $faker = \Faker\Factory::create('vi_VN');
    return [
        'mai_address' => $faker->unique()->safeEmail,
        'name' => $faker->name,
        'password'=> bcrypt('thanh1999'),
        'phone' => $faker->e164PhoneNumber,
        'address' => $faker->address,
    ];
});
