<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
    integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Login</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style type="text/css">
        span {
            color: red  ;
        }
    </style>
    <script>
        $(document).ready(function() {
            //jqueryemail
            @if ($errors->first('mai_address'))
            $("#ipemail").css("border", "1px solid red");
            $("#lbemail").css("color", "red");
            @endif
            //jqueryusername
            @if ($errors->first('name'))
            $("#ipusername").css("border", "1px solid red");
            $("#lbusername").css("color", "red");
            @endif
            //jqueryaddress
            @if ($errors->first('address'))
            $("#ipaddress").css("border", "1px solid red");
            $("#lbaddress").css("color", "red");
            @endif
            //jqueryphone
            @if ($errors->first('phone'))
            $("#ipphone").css("border", "1px solid red");
            $("#lbphone").css("color", "red");
            @endif
            //jquerypassword
            @if ($errors->first('password'))
            $("#ippassword").css("border", "1px solid red");
            $("#lbpassword").css("color", "red");
            @endif
            //jqueryconfirm
            @if ($errors->first('confirm_password'))
            $("#ipconfirm").css("border", "1px solid red");
            $("#lbconfirm").css("color", "red");
            @endif
        });
    </script>
</head>
<body>
    <div class="container">
        <h1>Add a new User</h1>
        <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col">
                    <label id="lbusername">Username:</label>
                    <input type="text" name="name" id="ipusername" class="form-control">
                    <span>
                    @if ($errors->first('name'))
                    {{ $errors->first('name') }}
                    @endif
                    </span>
                </div>
                <div class="col">
                    <label for="" id="lbemail">Email</label>
                    <input type="text" name="mai_address" id="ipemail" class="form-control">
                    <span>
                    @if ($errors->first('mai_address'))
                    {{ $errors->first('mai_address') }}
                    @endif
                </span>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="" id="lbpassword">Password</label>
                    <input type="password" name="password" id="ippassword" class="form-control">
                    <span>
                    @if ($errors->first('password'))
                    {{ $errors->first('password') }}
                    @endif
                    </span>
                </div>
                <div class="col">
                    <label for="" id="lbconfirm">Confirm Password</label>
                    <input type="password" name="confirm_password" id="ipconfirm" class="form-control">
                    <span>
                    @if ($errors->first('confirm_password'))
                    {{ $errors->first('confirm_password') }}
                    @endif
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="" id="lbaddress">Address</label>
                    <input type="text" name="address" id="ipaddress" class="form-control">
                    <span>
                    @if ($errors->first('address'))
                    {{ $errors->first('address') }}
                    @endif
                    </span>
                </div>
                <div class="col">
                    <label for="" id="lbphone">Phone</label>
                    <input type="text" name="phone" id="ipphone" class="form-control">
                    <span>
                    @if ($errors->first('phone'))
                    {{ $errors->first('phone') }}
                    @endif
                    </span>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <input type="submit" value="Add" class="btn btn-primary">
                </div>
            </div>               
        </form>
    </div>
</body>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous">
    </script>
</html>
