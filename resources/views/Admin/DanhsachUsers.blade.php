<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>
    @include('flash::message')
    <a href="{{route('user.create')}}" class="btn btn-primary">Add</a>
    <table class="table table-hover">
        <tr>
            <th>STT</th>
            <th>Name</th>
            <th>Email</th>
            <th>SDT</th>
            <th>Address</th>
            <th>Create At</th>
        </tr>
        @foreach ($users as $key => $us)
        <tr>
            <td>{{ 20 * $users->currentPage() - 19 + $key }}</td>
            <td>{{ $us->name }}</td>
            <td>{{ $us->mai_address }}</td>
            <td>{{ $us->phone }}</td>
            <td>{{ $us->address }}</td>
            <td>{{ $us->created_at }}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="5">{{ $users->links() }}</td>
        </tr>
    </table>
</body>
</html>
